<?php

namespace Drupal\entity_submit_redirect;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;

/**
 * Handles redirection after pressing redirect button.
 */
class EntitySubmitRedirectHandler {

  /**
   * Entity submit redirect configuration.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Route Match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Path validator object.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * @var \Drupal\token\TokenInterface
   */
  private $token;

  /**
   * Constructs Entity Submit Redirect.
   *
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration factory.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   Route match object.
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   Administration context.
   * @param \Drupal\Core\Path\PathValidatorInterface $pathValidator
   *   Drupal path validator.
   */
  public function __construct(ConfigFactory $configFactory, RouteMatchInterface $routeMatch, AdminContext $adminContext, PathValidatorInterface $pathValidator, Token $token = NULL) {
    $this->config = $configFactory->get('entity_submit_redirect.settings');
    $this->routeMatch = $routeMatch;
    $this->adminContext = $adminContext;
    $this->pathValidator = $pathValidator;
    $this->token = $token;
  }

  /**
   * Provides redirect URL for the given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *  Entity to get redirect URL object
   *
   * @return bool|\Drupal\Core\Url
   *   URL object or NULL if unable to redirect
   */
  public function getRedirectForEntity(ContentEntityInterface $entity): ?Url {
    $entityTypeId = $entity->getEntityTypeId();
    $bundle = $entity->bundle();

    $configs = [
      "$entityTypeId.$bundle.path",
      "$entityTypeId._default.path",
    ];

    // Check each config.
    foreach ($configs as $configId) {
      $redirect = $this->config->get($configId);
      if ($redirect) {
        $redirect = $this->token->replace($redirect, [$entityTypeId => $entity]);

        // Get URL object from the user input.
        $redirectUrl = $this->pathValidator->getUrlIfValid($redirect);
        if ($redirectUrl && is_a($redirectUrl, Url::class)) {
          // Provide first URL object.
          return $redirectUrl;
        }
      }
    }

    return NULL;
  }

  /**
   * Check global conditions for redirect action.
   *
   * @return bool
   *   TRUE if conditions passed, FALSE otherwise.
   */
  public function canRedirect(): bool {
    $result = TRUE;

    $useOn = $this->config->get('global.use_on');

    if ($useOn && count($useOn) !== 2) {
      $route = $this->routeMatch->getRouteObject();
      $isAdmin = $this->adminContext->isAdminRoute($route);

      if ($isAdmin) {
        $result = !empty($useOn['backoffice']);
      }
      else {
        $result = !empty($useOn['front']);
      }
    }

    return $result;
  }

  /**
   * Provides buttons list for the redirection.
   *
   * @todo Buttons must be configurable
   */
  public function getButtonsList(): array {
    return [
      'submit',
      'publish',
      'unpublish',
    ];
  }

}
